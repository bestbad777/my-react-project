import styled from "@emotion/styled";
import { useCallback } from "react";

export type TodoProps = {
  todoName?: string;
};

const TodoBox = styled.div`
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

const TodoItem = (props: TodoProps) => {
  const { todoName } = props;
  const onDoneClick = useCallback(() => {
    console.log("Done");
  }, []);

  const onEditClick = useCallback(() => {
    console.log("Edit");
  }, []);

  const onDeleteClick = useCallback(() => {
    console.log("Delete");
  }, []);

  return (
    <TodoBox>
      <button onClick={onDoneClick}>Done</button>
      <h3>{todoName}</h3>
      <button onClick={onEditClick}>Edit</button>
      <button onClick={onDeleteClick}>Delete</button>
    </TodoBox>
  );
};

export default TodoItem;
