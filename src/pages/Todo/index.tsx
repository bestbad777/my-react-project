import styled from "@emotion/styled";
import {
  Box,
  Button,
  Checkbox,
  TextField,
  Modal,
  Typography,
} from "@mui/material";
import { useCallback, useEffect, useMemo, useState } from "react";
import { Form, Field } from "react-final-form";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

// import TodoItem from "./component/TodoItem";

const Body = styled.div`
  width: 100%;
  margin-top: 150px;
  justify-content: center;
  align-items: center;
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const Div = styled.div`
  justify-content: center;
  align-items: center;
  margin-top: 20px;
  display: flex;
  flex: 1;
`;

const TodoBox = styled(Box)`
  margin-top: 10px;
  margin-bottom: 10px;
  min-width: 30%;
  padding-left: 15px;
  padding-right: 15px;
  background-color: #f3feff;
  text-align: center;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  display: flex;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

const ButtonBox = styled(Button)`
  height: 30px;
  width: 100px;
  margin: 5px;
  border-radius: 10px;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

const DisplayItemBox = styled.p`
  margin-left: 25px;
  margin-right: 25px;
  width: 150px;
`;

const DisplayTextDecorationLine = styled.p`
  margin-left: 25px;
  margin-right: 25px;
  width: 150px;
  text-decoration-line: line-through;
  text-decoration-color: red;
  text-decoration-thickness: 3px;
`;
const ModalBox = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  /* transform: translate(-50%, -50%); */
  width: 400;
  border: 2px solid #000;
  /* box-shadow: 24; */
  /* background-color: #818181; */
  padding: 15px;
`;

type TodoType = {
  //type ใช้ตัวใหญ่นำ
  todoName: string;
  id: number;
  isDone: boolean;
};

const Todo = () => {
  const [todoList, setTodoList] = useState<TodoType[]>([]);
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [editedId, setEditedId] = useState(0);

  useEffect(() => {
    console.log(editedId);
  }, []);

  const openModelHandle = useCallback(() => {
    setIsOpenModal(true);
  }, []);
  const closeModelHandle = useCallback(() => {
    setIsOpenModal(false);
  }, []);

  const onSubmit = (values: TodoType) => {
    setTodoList([
      ...todoList,
      { todoName: values.todoName, id: Date.now(), isDone: false },
    ]);
    // setIsOpenModal(true);
  };

  const debugAlert = () => {
    window.alert(JSON.stringify(todoList));
  };

  const TodoItem = (props: TodoType) => {
    const { todoName, id, isDone } = props;

    const handleEdit = (values: TodoType) => {
      setTodoList(
        todoList.map((props) => {
          return props.id === id
            ? { id: props.id, todoName: values.todoName, isDone: isDone }
            : props;
        })
      );
      setIsEdit(false);
    };

    const initialEditValues = useMemo(() => {
      return { todoName: todoName };
    }, []);

    const showValue = useCallback(() => {
      if (isEdit && editedId === id) {
        return (
          <DisplayItemBox>
            <Form<TodoType>
              onSubmit={handleEdit}
              initialValues={initialEditValues}
              subscription={{
                pristine: true,
              }}
              validate={(values: TodoType) => {
                const error: any = {};
                if (!values.todoName) {
                  error.todoName = "required";
                }
                if (`${values.todoName}`.length > 20) {
                  error.todoName = "must less than 20 characters";
                }
                return error;
              }}
            >
              {({ handleSubmit, pristine, values, hasValidationErrors }) => {
                return (
                  <form onSubmit={handleSubmit}>
                    <Field name="todoName">
                      {({ input, meta }) => (
                        <div>
                          <TextField
                            name={input.name}
                            value={input.value}
                            onChange={input.onChange}
                            fullWidth
                            id="fullWidth"
                            label="Todo"
                            variant="standard"
                            defaultValue=" "
                            size="medium"
                          />
                          <Div>
                            <Button
                              type="submit"
                              variant="contained"
                              // color="success"
                              disabled={hasValidationErrors || pristine}
                              onClick={handleSubmit}
                              size="small"
                            >
                              Submit
                            </Button>
                          </Div>
                          {meta.error && meta.touched && (
                            <span style={{ color: "red" }}>{meta.error}</span>
                          )}
                        </div>
                      )}
                    </Field>
                  </form>
                );
              }}
            </Form>
          </DisplayItemBox>
        );
      } else {
        if (!isDone) {
          return <DisplayItemBox>{todoName}</DisplayItemBox>;
        } else {
          return (
            <DisplayTextDecorationLine>{todoName}</DisplayTextDecorationLine>
          );
        }
      }
    }, []);

    const onDoneClick = useCallback(() => {
      setTodoList(
        todoList.map((props) => {
          return props.id === id
            ? { id: props.id, todoName: props.todoName, isDone: !props.isDone }
            : props;
        })
      );
    }, []);

    const onEditClick = useCallback(() => {
      setIsEdit(!isEdit);
      setEditedId(id);
      console.log("I am Edit!", editedId);
    }, []);

    const onDeleteClick = useCallback(() => {
      setTodoList(todoList.filter((props) => props.id !== id));
      console.log("Delete");
    }, []);

    return (
      <TodoBox>
        <ButtonBox color="success" onClick={onDoneClick}>
          <Checkbox size="small" checked={isDone} color="success" />
          Done
        </ButtonBox>
        {showValue()}
        <ButtonBox onClick={onEditClick} startIcon={<EditIcon />}>
          Edit
        </ButtonBox>
        <ButtonBox
          color="error"
          onClick={onDeleteClick}
          startIcon={<DeleteIcon />}
        >
          Delete
        </ButtonBox>
      </TodoBox>
    );
  };

  const UseModal = useCallback(() => {
    return (
      <Modal
        open={isOpenModal}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
        BackdropProps={{ style: { backgroundColor: "#ffc3c310" } }}
      >
        <ModalBox>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            success
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            this todo has been add to list
          </Typography>
          <Button onClick={closeModelHandle}>Close</Button>
        </ModalBox>
      </Modal>
    );
  }, []);

  const initialValues = useMemo(() => {
    return { todoName: "" };
  }, []);

  return (
    <Body>
      <Form<TodoType>
        onSubmit={onSubmit}
        initialValues={initialValues}
        subscription={{
          pristine: true,
        }}
        validate={(values: TodoType) => {
          const error: any = {};
          if (!values.todoName) {
            error.todoName = "required";
          }
          if (`${values.todoName}`.length > 20) {
            error.todoName = "must less than 20 characters";
          }
          return error;
        }}
      >
        {({ handleSubmit, pristine, values, hasValidationErrors }) => {
          return (
            <form onSubmit={handleSubmit}>
              <Field name="todoName">
                {/* {(props,{input, meta }) => ( */}

                {({ input, meta }) => (
                  <div>
                    <TextField
                      name={input.name}
                      value={input.value}
                      onChange={input.onChange}
                      fullWidth
                      id="fullWidth"
                      label="Todo"
                      variant="standard"
                      defaultValue=" "
                      size="medium"
                    />
                    {meta.error && meta.touched && (
                      <span style={{ color: "red" }}>{meta.error}</span>
                    )}
                  </div>
                )}
              </Field>
              <Div>
                <Button
                  type="submit"
                  variant="contained"
                  // color="success"
                  disabled={hasValidationErrors || pristine}
                  onClick={handleSubmit}
                >
                  Submit
                </Button>
              </Div>
              <Div>
                <Button variant="outlined" onClick={debugAlert}>
                  Dubug
                </Button>
              </Div>
              <pre>{JSON.stringify(values)}</pre>
              <UseModal />
            </form>
          );
        }}
      </Form>
      {todoList.map((todoItem) => {
        return (
          <TodoItem
            todoName={todoItem.todoName}
            id={todoItem.id}
            isDone={todoItem.isDone}
          />
        );
      })}
    </Body>
  );
};

export default Todo;