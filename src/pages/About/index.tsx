import styled from "@emotion/styled";
import WallPaperBg from "../../assets/Images/profile.png";
import Bg from "../../assets/Images/bg.jpg";

const BackgroundImage = styled.div`
  background-image: url(${Bg});
  background-size: cover;
  margin-top: 80px;
  width: auto;
  height: auto;
  align-items: center;
  justify-content: center;
  display: flex;
  flex: 1;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

const ImgProfile = styled.img`
  background-color: blue;
  position: relative;
  margin: 1%;
  right:8%;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

const TxtArea1 = styled.div`
  display: block;
  color: #ff3b86;
  margin-top: 5%;
  margin-left: 2.5%;
  width: 100%;
  text-align: center;
  align-items: center;
  justify-content: center;
  text-shadow: 2px 2px 4 px #000000;
  font-family: "Lucida Handwriting";
  font-size: 200%;
  line-height: 1;
`;

const TxtArea2 = styled.div`
  width: 100%;
  margin-top: 5%;
  margin-left: 2.5%;
  /* text-indent: 10%; */
  color: #000000;
  text-shadow: 2px 2px 2px #ffaded;
`;

const Div = styled.div`
  display: block;
`;

const About = () => {
  return (
    <BackgroundImage>
      <ImgProfile src={WallPaperBg} />
      <div>
        <TxtArea1>
          <h3>Hello, I am</h3>
          <h1>Zarayut Ketsugziri</h1>
          <h4>Software Engineer</h4>
        </TxtArea1>
        <TxtArea2>
          <h2>
            It’s Zarayut Ketsugziri, I’m a junior programmer. I always looking
            for exciting projects.
          </h2>
        </TxtArea2>
      </div>
    </BackgroundImage>
    // style={{ marginLeft: "15%" ,flex:2}}
  );
};

export default About;
