import styled from "@emotion/styled";
import React from "react";

const Body = styled.div`
  margin-top: 100px;
  justify-content: center;
  display: flex;
  width: 100%;
`;

const Home = () => {
  return (
    <Body>
      <h1>Not have any content this time.</h1>
    </Body>
  );
};

export default Home;
