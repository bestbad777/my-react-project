export type TodoType = {
  //type ใช้ตัวใหญ่นำ
  todoName: string;
  id: number;
  isDone: boolean;
};
