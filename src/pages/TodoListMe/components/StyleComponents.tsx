import { Box, Button } from "@mui/material";
import styled from "@emotion/styled";

export const Body = styled.div`
  width: 100%;
  margin-top: 150px;
  justify-content: center;
  align-items: center;
  display: flex;
  flex: 1;
  flex-direction: column;
`;

export const Div = styled.div`
  justify-content: center;
  align-items: center;
  margin-top: 20px;
  display: flex;
  flex: 1;
`;

export const TodoBox = styled(Box)`
  margin-top: 10px;
  margin-bottom: 10px;
  min-width: 30%;
  padding-left: 15px;
  padding-right: 15px;
  background-color: #f3feff;
  text-align: center;
  justify-content: center;
  align-items: center;
  border-radius: 15px;
  display: flex;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

export const ButtonBox = styled(Button)`
  height: 30px;
  width: 100px;
  margin: 5px;
  border-radius: 10px;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

export const DisplayItemBox = styled.p`
  margin-left: 25px;
  margin-right: 25px;
  width: 150px;
`;

export const DisplayTextDecorationLine = styled.p`
  margin-left: 25px;
  margin-right: 25px;
  width: 150px;
  text-decoration-line: line-through;
  text-decoration-color: red;
  text-decoration-thickness: 3px;
`;
export const ModalBox = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  /* transform: translate(-50%, -50%); */
  width: 400;
  border: 2px solid #000;
  /* box-shadow: 24; */
  /* background-color: #818181; */
  padding: 15px;
`;

export const Hr = styled.hr`
  background-color: #9e9eff;
  height: 1px;
  width: 40%;
  margin-top: 40px;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;