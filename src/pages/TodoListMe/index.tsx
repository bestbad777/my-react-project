import styled from "@emotion/styled";
import {
  Box,
  Button,
  Checkbox,
  TextField,
  Modal,
  Typography,
} from "@mui/material";
import { useCallback, useEffect, useMemo, useState } from "react";
import { Form, Field } from "react-final-form";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import { Card } from "./components/Card";
import update from "immutability-helper";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import {
  Body,
  ButtonBox,
  DisplayItemBox,
  DisplayTextDecorationLine,
  Div,
  TodoBox,
} from "./components/StyleComponents";
import { TodoType } from "./components/Type";

const TodoListMe = () => {
  const [todoList, setTodoList] = useState<TodoType[]>([]);
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [editedId, setEditedId] = useState<number>(0);

  const moveCard = useCallback(
    (dragIndex: number, hoverIndex: number) => {
      const dragCard = todoList[dragIndex];
      setTodoList(
        update(todoList, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
    },
    [todoList]
  );

  const renderCard = (
    card: { id: number; todoName: string },
    index: number
  ) => {
    return (
      <Card
        key={card.id}
        index={index}
        id={card.id}
        text={card.todoName}
        moveCard={moveCard}
      />
    );
  };

  const TodoItem = (props: TodoType) => {
    const { todoName, id, isDone } = props;

    const handleEdit = (values: TodoType) => {
      setTodoList(
        todoList.map((props) => {
          return props.id === id
            ? { id: props.id, todoName: values.todoName, isDone: isDone }
            : props;
        })
      );
      setIsEdit(false);
    };

    const initialEditValues = useMemo(() => {
      return { todoName: todoName };
    }, []);

    const showValue = useCallback(() => {
      if (isEdit && editedId === id) {
        return (
          <DisplayItemBox>
            <Form<TodoType>
              onSubmit={handleEdit}
              initialValues={initialEditValues}
              subscription={{
                pristine: true,
              }}
              validate={(values: TodoType) => {
                const error: any = {};
                if (!values.todoName) {
                  error.todoName = "required";
                }
                if (`${values.todoName}`.length > 20) {
                  error.todoName = "must less than 20 characters";
                }
                return error;
              }}
            >
              {({ handleSubmit, pristine, values, hasValidationErrors }) => {
                return (
                  <form onSubmit={handleSubmit}>
                    <Field name="todoName">
                      {({ input, meta }) => (
                        <div>
                          <TextField
                            name={input.name}
                            value={input.value}
                            onChange={input.onChange}
                            fullWidth
                            id="fullWidth"
                            label="Todo"
                            variant="standard"
                            defaultValue=" "
                            size="medium"
                          />
                          <Div>
                            <Button
                              type="submit"
                              variant="contained"
                              // color="success"
                              disabled={hasValidationErrors || pristine}
                              onClick={handleSubmit}
                              size="small"
                            >
                              Submit
                            </Button>
                          </Div>
                          {meta.error && meta.touched && (
                            <span style={{ color: "red" }}>{meta.error}</span>
                          )}
                        </div>
                      )}
                    </Field>
                  </form>
                );
              }}
            </Form>
          </DisplayItemBox>
        );
      } else {
        if (!isDone) {
          return <DisplayItemBox>{todoName}</DisplayItemBox>;
        } else {
          return (
            <DisplayTextDecorationLine>{todoName}</DisplayTextDecorationLine>
          );
        }
      }
    }, []);

    const onDoneClick = useCallback(() => {
      setTodoList(
        todoList.map((props) => {
          return props.id === id
            ? { id: props.id, todoName: props.todoName, isDone: !props.isDone }
            : props;
        })
      );
    }, []);

    const onEditClick = useCallback(() => {
      setIsEdit(!isEdit);
      setEditedId(id);
      console.log("I am Edit!", editedId);
    }, []);

    const onDeleteClick = useCallback(() => {
      setTodoList(todoList.filter((props) => props.id !== id));
      console.log("Delete");
    }, []);

    return (
      <TodoBox>
        <ButtonBox color="success" onClick={onDoneClick}>
          <Checkbox size="small" checked={isDone} color="success" />
          Done
        </ButtonBox>
        {showValue()}
        <ButtonBox onClick={onEditClick} startIcon={<EditIcon />}>
          Edit
        </ButtonBox>
        <ButtonBox
          color="error"
          onClick={onDeleteClick}
          startIcon={<DeleteIcon />}
        >
          Delete
        </ButtonBox>
      </TodoBox>
    );
  };

  const onSubmit = (values: TodoType) => {
    setTodoList([
      ...todoList,
      { todoName: values.todoName, id: Date.now(), isDone: false },
    ]);
  };

  const debugAlert = () => {
    window.alert(JSON.stringify(todoList));
  };
  
  const initialValues = useMemo(() => {
    return { todoName: "" };
  }, []);

  return (
    <Body>
      <Form<TodoType>
        onSubmit={onSubmit}
        initialValues={initialValues}
        subscription={{
          pristine: true,
        }}
        validate={(values: TodoType) => {
          const error: any = {};
          if (!values.todoName) {
            error.todoName = "required";
          }
          if (`${values.todoName}`.length > 20) {
            error.todoName = "must less than 20 characters";
          }
          return error;
        }}
      >
        {({ handleSubmit, pristine, values, hasValidationErrors }) => {
          return (
            <form onSubmit={handleSubmit}>
              <Field name="todoName">
                {/* {(props,{input, meta }) => ( */}

                {({ input, meta }) => (
                  <div>
                    <TextField
                      name={input.name}
                      value={input.value}
                      onChange={input.onChange}
                      fullWidth
                      id="fullWidth"
                      label="Todo"
                      variant="standard"
                      defaultValue=" "
                      size="medium"
                    />
                    {meta.error && meta.touched && (
                      <span style={{ color: "red" }}>{meta.error}</span>
                    )}
                  </div>
                )}
              </Field>
              <Div>
                <Button
                  type="submit"
                  variant="contained"
                  // color="success"
                  disabled={hasValidationErrors || pristine}
                  onClick={handleSubmit}
                >
                  Submit
                </Button>
              </Div>
              <Div>
                <Button variant="outlined" onClick={debugAlert}>
                  Dubug
                </Button>
              </Div>
            </form>
          );
        }}
      </Form>
      <DndProvider backend={HTML5Backend}>
        <div>{todoList.map((card, i) => renderCard(card, i))}</div>
      </DndProvider>

      {/* {todoList.map((todoItem) => {
        if (todoItem.isDone === false) {
          return (
            <TodoItem
              todoName={todoItem.todoName}
              id={todoItem.id}
              isDone={todoItem.isDone}
            />
          );
        }
      })}
      <Hr />
      <h2>Done List</h2>
      {todoList.map((todoItem) => {
        if (todoItem.isDone === true) {
          return (
            <TodoItem
              todoName={todoItem.todoName}
              id={todoItem.id}
              isDone={todoItem.isDone}
            />
          );
        }
      })} */}
      {/* <Hr />
      <h2>Sort List</h2> */}
    </Body>
  );
};

export default TodoListMe;
