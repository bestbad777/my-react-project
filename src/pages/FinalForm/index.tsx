import React, { useState } from "react";
import { Field, Form } from "react-final-form";
import styled from "styled-components";
import banner from "../../assets/Images/banner.png";
import { Button, Checkbox, FormControlLabel, TextField } from "@mui/material";

const ImageArea = styled.div`
  background-color: #76c5db;
  margin-top: 5px;
  align-items: center;
  justify-content: center;
  display: flex;
  flex: 1;
`;

const Div = styled.div`
  margin-top: 300px;
  align-items: center;
  text-align: center;
  justify-content: center;
  display: flex;
  flex: 1;
`;

const DivArea = styled.div`
  margin: 10px;
  align-items: center;
  text-align: center;
  justify-content: center;
`;

const GridLayout = styled.div`
  margin: 10px;
  margin-bottom: 25px;
  align-items: center;
  text-align: center;
  justify-content: center;
  display: grid;
  grid-template-columns: 100px 100px;
  grid-column-gap: 10px;
  grid-row-gap: 10px;
`;

const Grid = styled.div`
  margin: 10px;
  align-items: center;
  text-align: center;
  justify-content: center;
  display: grid;
  grid-template-columns: 150px 150px;
  grid-column-gap: 10px;
  grid-row-gap: 10px;
`;

const sleep = (ms: any) => new Promise((resolve) => setTimeout(resolve, ms));

const onSubmit = async (values: any) => {
  await sleep(300);
  window.alert(JSON.stringify(values));
  console.log(values);
};

const FinalForm = (props: any) => {
  const [like, setLike] = useState(0);

  let formData = {};

  return (
    <Div>
      {/* <ImageArea>
        <img src={banner}></img>
      </ImageArea>
      <DivArea>
        <h1>Like : {like}</h1>
        <GridLayout>
          <Button
            variant="outlined"
            size="medium"
            onClick={() => setLike(like + 1)}
          >
            Like
          </Button>
          <Button
            variant="outlined"
            size="medium"
            onClick={() => setLike(like - 1)}
          >
            Dislike
          </Button>
        </GridLayout>
      </DivArea> */}
      <Form
        onSubmit={onSubmit}
        initialValues={{
          ...formData,
        }}
        render={({ handleSubmit, form, submitting, pristine, values }) => (
          <form onSubmit={handleSubmit}>
            <Grid style={{ gridColumn: "10px" }}>
              <Field name="First Name">
                {(props) => (
                  <div>
                    <TextField
                      name={props.input.name}
                      value={props.input.value}
                      onChange={props.input.onChange}
                      id="outlined-required"
                      label="First Name"
                      defaultValue=" "
                      size="small"
                    />
                  </div>
                )}
              </Field>
              <Field name="Last Name">
                {(props) => (
                  <div>
                    <TextField
                      name={props.input.name}
                      value={props.input.value}
                      onChange={props.input.onChange}
                      id="outlined-required"
                      label="Last Name"
                      defaultValue=" "
                      size="small"
                    />
                  </div>
                )}
              </Field>
            </Grid>

            <Field name="isManUFan">
              {(props) => (
                <div>
                  <FormControlLabel
                    control={
                      <Checkbox
                        value={props.input.value}
                        onChange={props.input.onChange}
                        color="success"
                      />
                    }
                    label="Manchester United Fan"
                  />
                </div>
              )}
            </Field>
            <DivArea>
              <GridLayout>
                <Button
                  type="submit"
                  variant="contained"
                  color="success"
                  // disabled={submitting || pristine}
                >
                  Submit
                </Button>
                <Button
                  type="button"
                  variant="outlined"
                  color="error"
                  onClick={form.reset}
                  // disabled={submitting || pristine}
                >
                  Reset
                </Button>
              </GridLayout>
            </DivArea>
            <pre>{JSON.stringify(values)}</pre>
            {/* <pre>{JSON.stringify(values, 0, 2)}</pre> */}
          </form>
        )}
      />
    </Div>
  );
};

export default FinalForm;

{
  /* <DivArea>
              <label>Favorite Color</label>
              <Field name="favoriteColor" component="select">
                <option />
                <option value="#ff0000">❤️ Red</option>
                <option value="#00e700">💚 Green</option>
                <option value="#0000ff">💙 Blue</option>
              </Field>
            </DivArea>
            <DivArea>
              <label>Favorite Team</label>
              <Field name="fav_team" component="select" multiple>
                <option value="Man United">Man United</option>
                <option value="Man City">Man City</option>
                <option value="Liverpool">Liverpool</option>
              </Field>
            </DivArea>
            <DivArea>
              <label>Formation</label>
              <DivArea>
                <label>
                  <Field
                    name="fav_formation"
                    component="input"
                    type="checkbox"
                    value="4-4-2"
                  />{" "}
                  4-4-2
                </label>
                <label>
                  <Field
                    name="fav_formation"
                    component="input"
                    type="checkbox"
                    value="4-2-3-1"
                  />{" "}
                  4-2-3-1
                </label>
                <label>
                  <Field
                    name="fav_formation"
                    component="input"
                    type="checkbox"
                    value="4-3-3"
                  />{" "}
                  4-3-3
                </label>
              </DivArea>
            </DivArea>
            <DivArea>
              <label>Favorite Player</label>

              <label>
                <Field
                  name="fav_player"
                  component="input"
                  type="radio"
                  value="ronaldo"
                />
                {""}
                Ronaldo
              </label>
              <label>
                <Field
                  name="fav_player"
                  component="input"
                  type="radio"
                  value="messi"
                />
                {""}
                Messi
              </label>
            </DivArea>
            <DivArea>
              <div>
                <label>Notes</label>
                <Field name="notes" component="textarea" placeholder="Notes" />
              </div>
            </DivArea> */
}
