export type CreateTodoParams = {
  id: number;
  name: string;
  isCompleted: boolean;
  deleted_at: null;
};
