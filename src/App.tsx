import { BrowserRouter, Route, Switch, Link } from "react-router-dom";
import Home from "./pages/Home";
import FinalForm from "./pages/FinalForm";
import Todo from "./pages/Todo";
import About from "./pages/About";
import styled from "@emotion/styled";
import { Box, Button } from "@mui/material";
import MailIcon from "@mui/icons-material/Mail";
import CallIcon from "@mui/icons-material/Call";
import HomeIcon from "@mui/icons-material/Home";
import TodoListMe from "./pages/TodoListMe";

const Header = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  justify-content: center;
  display: flex;
  background-color: #e1fffc;
  padding-top: 10px;
  padding-bottom: 10px;
  width: 100%;
  height: 50px;
  z-index: 100;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

const Footer = styled.div`
  position: fixed;
  left: 0;
  bottom: 0;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  display: flex;
  background-color: #e1fffc;
  margin-top: 10px;
  padding-top: 10px;
  padding-bottom: 10px;
  width: 100%;
  height: 50px;
  z-index: 80;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

const RouteBox = styled(Button)`
  height: 50px;
  width: auto;
  padding: 10px;
  margin-left: 25px;
  margin-right: 25px;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25));
`;

const App = () => {
  return (
    <BrowserRouter>
      <Header>
        <Link to="/" style={{ textDecoration: "none" }}>
          <RouteBox>
            <HomeIcon />
            <h3 style={{ textIndent: "5px" }}>Home</h3>
          </RouteBox>
        </Link>
        <Link to="/about" style={{ textDecoration: "none" }}>
          <RouteBox>
            <h3>About</h3>
          </RouteBox>
        </Link>
        <Link to="/final-form" style={{ textDecoration: "none" }}>
          <RouteBox>
            <h3>Final Form</h3>
          </RouteBox>
        </Link>
        <Link to="/todo-list" style={{ textDecoration: "none" }}>
          <RouteBox>
            <h3>ToDoList</h3>
          </RouteBox>
        </Link>
        <Link to="/todo-list-me" style={{ textDecoration: "none" }}>
          <RouteBox>
            <h3>ToDoListME</h3>
          </RouteBox>
        </Link>
      </Header>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/final-form" component={FinalForm} />
        <Route path="/todo-list" component={Todo} />
        <Route path="/todo-list-me" component={TodoListMe} />
      </Switch>
      <Footer>
        <RouteBox>
          <CallIcon />
          <h4 style={{ textIndent: "5px" }}>0982647185</h4>
        </RouteBox>
        <RouteBox>
          <MailIcon />
          <h4 style={{ textIndent: "5px" }}>Bestbad777@gmail.com</h4>
        </RouteBox>
      </Footer>
    </BrowserRouter>
  );
};

export default App;
